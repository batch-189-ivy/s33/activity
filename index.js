

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))

/*fetch('https://jsonplaceholder.typicode.com/todos/1')
			.then((response) => response.json())
			.then((data) => {
				console.log(data.map(toDo => {
					console.log("The item " + toDo.title + " on the list has a status of " + toDo.completed)
			}));
		});*/


fetch('https://jsonplaceholder.typicode.com/todos?id=1')
			.then((response) => response.json())
			.then((data) => {
				console.log(data.map(toDo => {
					console.log("The item " + toDo.title + " on the list has a status of " + toDo.completed)
			}));
		});

/*fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data.map(toDo => {
		return toDo.title;
	}));
});
*/


/*
Code ni sir earl

let todo_titles

fetch('https://jsonplaceholder.typicode.com/todos/1')
			.then((response) => response.json())
			.then((data) => {
				todo_titles = data.map((todo) => {
					return todo.title;
				}))
			})
			.then(() => console.log(todo_titles))

*/


/*
Code ni sir earl

let todo_titles

async function getToDoTitles(){
	let result = await fetch('https://jsonplaceholder.typicode.com/todos/1')
	let todos = await result.json()

	todo_titles = todos.map(todo => {
		return todo.title
	})
}

getTodoTitles()
console.log(todo_titles)


*/


fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'Created To do List Item',
		completed: false
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status:"Pending",
		title: 'Updated To do List Item',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))




let today = new Date();

let date = `${today.getFullYear()}-${today.getMonth()+1}-${today.getDate()}`;

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: date,
		id: 1,
		status:"Complete",
		title: 'delectus aut autem',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',

})
.then((response) => response.json())
.then((data) => console.log(data))

